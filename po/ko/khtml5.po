# Korean messages for kdelibs.
# Copyright (C) Free Software Foundation, Inc.
# Cho Sung Jae <cho.sungjae@gmail.com>, 2007.
# Shinjo Park <kde@peremen.name>, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2019, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdelibs4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 22:21+0000\n"
"PO-Revision-Date: 2022-05-15 22:25+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "조성재,박신조"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "cho.sungjae@gmail.com,kde@peremen.name"

#: ecma/debugger/callstackdock.cpp:35
#, kde-format
msgid "Call Stack"
msgstr "호출 스택"

#: ecma/debugger/callstackdock.cpp:39
#, kde-format
msgid "Call"
msgstr "호출"

#: ecma/debugger/callstackdock.cpp:39
#, kde-format
msgid "Line"
msgstr "줄"

#: ecma/debugger/consoledock.cpp:220
#, kde-format
msgid "Console"
msgstr "콘솔"

#: ecma/debugger/consoledock.cpp:243
#, kde-format
msgid "Enter"
msgstr "들어가기"

#: ecma/debugger/debugdocument.cpp:203
#, kde-format
msgid ""
"Unable to find the Kate editor component;\n"
"please check your KDE installation."
msgstr ""
"Kate 편집기 구성 요소를 찾을 수 없습니다.\n"
"KDE 설치를 점검해 주십시오."

#: ecma/debugger/debugdocument.cpp:269
#, kde-format
msgid "Breakpoint"
msgstr "중단점"

#: ecma/debugger/debugwindow.cpp:96
#, kde-format
msgid "JavaScript Debugger"
msgstr "자바스크립트 디버거"

#: ecma/debugger/debugwindow.cpp:173
#, kde-format
msgid "&Break at Next Statement"
msgstr "다음 명령어에서 멈춤(&B)"

#: ecma/debugger/debugwindow.cpp:174
#, kde-format
msgid "Break at Next"
msgstr "다음 명령어에서 멈춤"

#: ecma/debugger/debugwindow.cpp:179
#, kde-format
msgid "Continue"
msgstr "계속"

#: ecma/debugger/debugwindow.cpp:185
#, kde-format
msgid "Step Over"
msgstr "지나가기"

#: ecma/debugger/debugwindow.cpp:191
#, kde-format
msgid "Step Into"
msgstr "진행하기"

#: ecma/debugger/debugwindow.cpp:198
#, kde-format
msgid "Step Out"
msgstr "나가기"

#: ecma/debugger/debugwindow.cpp:204
#, kde-format
msgid "Reindent Sources"
msgstr "소스 코드 다시 들여쓰기"

#: ecma/debugger/debugwindow.cpp:209
#, kde-format
msgid "Report Exceptions"
msgstr "예외 보고하기"

#: ecma/debugger/debugwindow.cpp:217
#, kde-format
msgid "&Debug"
msgstr "디버그(&D)"

#: ecma/debugger/debugwindow.cpp:225
#, kde-format
msgid "&Settings"
msgstr "설정(&S)"

#: ecma/debugger/debugwindow.cpp:251
#, kde-format
msgid "Close source"
msgstr "소스 닫기"

#: ecma/debugger/debugwindow.cpp:257
#, kde-format
msgid "Ready"
msgstr "준비"

#: ecma/debugger/debugwindow.cpp:582
#, kde-format
msgid ""
"An error occurred while attempting to run a script on this page.\n"
"\n"
"%1 line %2:\n"
"%3"
msgstr ""
"이 페이지의 스크립트를 실행하려는 중 오류가 발생했습니다.\n"
"\n"
"행 %2 %1:\n"
"%3"

#: ecma/debugger/debugwindow.cpp:775
#, kde-format
msgid ""
"Do not know where to evaluate the expression. Please pause a script or open "
"a source file."
msgstr ""
"표현식을 평가할 위치를 찾을 수 없습니다. 스크립트를 일시정지 시키거나 소스 파"
"일을 열어 주십시오."

#: ecma/debugger/debugwindow.cpp:809
#, kde-format
msgid "Evaluation threw an exception %1"
msgstr "평가 과정에서 예외 %1이(가) 발생했습니다"

#: ecma/debugger/errordlg.cpp:35
#, kde-format
msgid "JavaScript Error"
msgstr "자바스크립트 오류"

#: ecma/debugger/errordlg.cpp:49
#, kde-format
msgid "&Do not show this message again"
msgstr "이 메시지를 다시 표시하지 않기(&D)"

#: ecma/debugger/localvariabledock.cpp:43
#, kde-format
msgid "Local Variables"
msgstr "로컬 변수"

#: ecma/debugger/localvariabledock.cpp:51
#, kde-format
msgid "Reference"
msgstr "참조"

#. i18n: ectx: property (text), widget (QTreeWidget, _headers)
#: ecma/debugger/localvariabledock.cpp:52 htmlpageinfo.ui:219
#, kde-format
msgid "Value"
msgstr "값"

#: ecma/debugger/scriptsdock.cpp:37
#, kde-format
msgid "Loaded Scripts"
msgstr "불러온 스크립트"

#: ecma/kjs_binding.cpp:195
#, kde-format
msgid ""
"A script on this page is causing KHTML to freeze. If it continues to run, "
"other applications may become less responsive.\n"
"Do you want to stop the script?"
msgstr ""
"이 페이지의 스크립트 때문에 KHTML이 멈추었습니다. 만약 계속 실행한다면 다른 "
"프로그램의 응답이 느려집니다.\n"
"스크립트를 중지하시겠습니까?"

#: ecma/kjs_binding.cpp:195
#, kde-format
msgid "JavaScript"
msgstr "자바스크립트"

#: ecma/kjs_binding.cpp:195
#, kde-format
msgid "&Stop Script"
msgstr "스크립트 중지(&S)"

#: ecma/kjs_binding.cpp:445
#, kde-format
msgid "Parse error at %1 line %2"
msgstr "행 %2, %1에서 해석 오류"

#: ecma/kjs_html.cpp:2233 ecma/kjs_window.cpp:1944
#, kde-format
msgid "Confirmation: JavaScript Popup"
msgstr "확인: 자바스크립트 팝업"

#: ecma/kjs_html.cpp:2235
#, kde-format
msgid ""
"This site is submitting a form which will open up a new browser window via "
"JavaScript.\n"
"Do you want to allow the form to be submitted?"
msgstr ""
"이 사이트에서 자바스크립트를 통해 새 브라우저 창을 열도록 하는 폼을 전달하고 "
"있습니다.\n"
"폼을 전달하시겠습니까?"

#: ecma/kjs_html.cpp:2238
#, kde-format
msgid ""
"<qt>This site is submitting a form which will open <p>%1</p> in a new "
"browser window via JavaScript.<br />Do you want to allow the form to be "
"submitted?</qt>"
msgstr ""
"<qt>이 사이트에서 자바스크립트를 통해 새 브라우저 창에서 <p>%1</p>(을)를 열려"
"고 하는 폼을 전달하고 있습니다.<br />폼을 전달하시겠습니까?</qt>"

#: ecma/kjs_html.cpp:2240 ecma/kjs_window.cpp:1952
#, kde-format
msgid "Allow"
msgstr "허용"

#: ecma/kjs_html.cpp:2240 ecma/kjs_window.cpp:1952
#, kde-format
msgid "Do Not Allow"
msgstr "허용하지 않음"

#: ecma/kjs_window.cpp:1947
#, kde-format
msgid ""
"This site is requesting to open up a new browser window via JavaScript.\n"
"Do you want to allow this?"
msgstr ""
"이 사이트에서 자바스크립트를 통해 새 브라우저 창을 열려고 요청했습니다.\n"
"허용하시겠습니까?"

#: ecma/kjs_window.cpp:1950
#, kde-format
msgid ""
"<qt>This site is requesting to open<p>%1</p>in a new browser window via "
"JavaScript.<br />Do you want to allow this?</qt>"
msgstr ""
"<qt>이 사이트에서 자바스크립트를 통해 <p>%1</p>(을)를 새 브라우저 창에 열려"
"고 요청했습니다.<br />허용하시겠습니까?</qt>"

#: ecma/kjs_window.cpp:2283
#, kde-format
msgid "Close window?"
msgstr "창을 닫으시겠습니까?"

#: ecma/kjs_window.cpp:2283
#, kde-format
msgid "Confirmation Required"
msgstr "확인 필요"

#: ecma/kjs_window.cpp:3233
#, kde-format
msgid ""
"Do you want a bookmark pointing to the location \"%1\" to be added to your "
"collection?"
msgstr "위치 \"%1\"(을)를 가리키는 책갈피를 책갈피 목록에 추가하시겠습니까?"

#: ecma/kjs_window.cpp:3236
#, kde-format
msgid ""
"Do you want a bookmark pointing to the location \"%1\" titled \"%2\" to be "
"added to your collection?"
msgstr ""
"제목이 \"%2\"인 위치 \"%1\"을(를) 가리키는 책갈피를 책갈피 목록에 추가하시겠"
"습니까?"

#: ecma/kjs_window.cpp:3245
#, kde-format
msgid "JavaScript Attempted Bookmark Insert"
msgstr "자바스크립트로 책갈피를 추가하려는 중"

#: ecma/kjs_window.cpp:3249
#, kde-format
msgid "Insert"
msgstr "추가하기"

#: ecma/kjs_window.cpp:3249
#, kde-format
msgid "Disallow"
msgstr "허용하지 않음"

#: html/html_formimpl.cpp:420
#, kde-format
msgid ""
"The following files will not be uploaded because they could not be found.\n"
"Do you want to continue?"
msgstr ""
"다음의 파일을 찾을 수 없어서 업로드하지 않습니다.\n"
"계속하시겠습니까?"

#: html/html_formimpl.cpp:424
#, kde-format
msgid "Submit Confirmation"
msgstr "전송 확인"

#: html/html_formimpl.cpp:424
#, kde-format
msgid "&Submit Anyway"
msgstr "그래도 전송(&S)"

#: html/html_formimpl.cpp:434
#, kde-format
msgid ""
"You are about to transfer the following files from your local computer to "
"the Internet.\n"
"Do you really want to continue?"
msgstr ""
"다음 파일을 컴퓨터에서 인터넷으로 전송하려고 합니다.\n"
"계속하시겠습니까?"

#: html/html_formimpl.cpp:438
#, kde-format
msgid "Send Confirmation"
msgstr "보내기 확인"

#: html/html_formimpl.cpp:438
#, kde-format
msgid "&Send File"
msgid_plural "&Send Files"
msgstr[0] "파일 보내기(&S)"

#: html/html_formimpl.cpp:1687 html/html_formimpl.cpp:1908 khtml_part.cpp:4962
#: khtmlview.cpp:2848 khtmlview.cpp:2892
#, kde-format
msgid "Submit"
msgstr "보내기"

#: html/html_formimpl.cpp:1900 khtmlview.cpp:2863 khtmlview.cpp:2898
#, kde-format
msgid "Reset"
msgstr "초기화"

#: html/html_formimpl.cpp:2824
#, kde-format
msgid "Key Generator"
msgstr "키 생성기"

#: html/html_objectimpl.cpp:632
#, kde-format
msgid ""
"No plugin found for '%1'.\n"
"Do you want to download one from %2?"
msgstr ""
"'%1'용 플러그인을 찾을 수 없습니다.\n"
"%2 사이트에서 다운로드하시겠습니까?"

#: html/html_objectimpl.cpp:633
#, kde-format
msgid "Missing Plugin"
msgstr "사용 가능하지 않은 플러그인"

#: html/html_objectimpl.cpp:633
#, kde-format
msgid "Download"
msgstr "다운로드"

#: html/html_objectimpl.cpp:633
#, kde-format
msgid "Do Not Download"
msgstr "다운로드하지 않음"

#: html/htmlparser.cpp:1980
#, kde-format
msgid "This is a searchable index. Enter search keywords: "
msgstr "이것은 검색할 수 있는 색인입니다. 찾을 단어를 입력하십시오: "

#. i18n: ectx: property (text), widget (QLabel, TextLabel1)
#: html/keygenwizard.ui:35
#, kde-format
msgid ""
"You have indicated that you wish to obtain or purchase a secure certificate. "
"This wizard is intended to guide you through the procedure. You may cancel "
"at any time, and this will abort the transaction."
msgstr ""
"인증서를 구입하거나 가져오기로 했습니다. 이 마법사는 전체 과정을 안내합니다. "
"언제든지 이 단계를 취소할 수 있으며, 전체 트랜잭션이 중단됩니다."

#. i18n: ectx: property (text), widget (QLabel, TextLabel4)
#: html/keygenwizard2.ui:35
#, kde-format
msgid ""
"You must now provide a password for the certificate request. Please choose a "
"very secure password as this will be used to encrypt your private key."
msgstr ""
"인증 요청의 암호를 입력해야 합니다. 이 암호로 개인 키를 암호화하기 때문에 충"
"분히 강력한 암호를 입력하십시오."

#. i18n: ectx: property (text), widget (QLabel, TextLabel6)
#: html/keygenwizard2.ui:48
#, kde-format
msgid "&Repeat password:"
msgstr "암호 반복(&R):"

#. i18n: ectx: property (text), widget (QLabel, TextLabel5)
#: html/keygenwizard2.ui:58
#, kde-format
msgid "&Choose password:"
msgstr "암호 선택(&C):"

#: html/ksslkeygen.cpp:82
#, kde-format
msgid "KDE Certificate Request"
msgstr "KDE 인증서 요청"

#: html/ksslkeygen.cpp:89
#, kde-format
msgid "KDE Certificate Request - Password"
msgstr "KDE 인증서 요청 - 암호"

#: html/ksslkeygen.cpp:126
#, kde-format
msgid "Unsupported key size."
msgstr "지원하지 않는 키 크기입니다."

#: html/ksslkeygen.cpp:126
#, kde-format
msgid "KDE SSL Information"
msgstr "KDE SSL 정보"

#: html/ksslkeygen.cpp:132 khtml_part.cpp:5017
#, kde-format
msgid "KDE"
msgstr "KDE"

#: html/ksslkeygen.cpp:133
#, kde-format
msgid "Please wait while the encryption keys are generated..."
msgstr "암호화 키를 만드는 동안 기다려 주십시오..."

#: html/ksslkeygen.cpp:148
#, kde-format
msgid "Do you wish to store the passphrase in your wallet file?"
msgstr "지갑 파일에 암호를 저장하시겠습니까?"

#: html/ksslkeygen.cpp:148
#, kde-format
msgid "Store"
msgstr "저장"

#: html/ksslkeygen.cpp:148
#, kde-format
msgid "Do Not Store"
msgstr "저장하지 않음"

#: html/ksslkeygen.cpp:257
#, kde-format
msgid "2048 (High Grade)"
msgstr "2048 (높은 등급)"

#: html/ksslkeygen.cpp:258
#, kde-format
msgid "1024 (Medium Grade)"
msgstr "1024 (중간 등급)"

#: html/ksslkeygen.cpp:259
#, kde-format
msgid "768  (Low Grade)"
msgstr "768  (낮은 등급)"

#: html/ksslkeygen.cpp:260
#, kde-format
msgid "512  (Low Grade)"
msgstr "512  (낮은 등급)"

#: html/ksslkeygen.cpp:262
#, kde-format
msgid "No SSL support."
msgstr "SSL 지원 없음."

#. i18n: ectx: property (windowTitle), widget (QDialog, KHTMLInfoDlg)
#: htmlpageinfo.ui:22
#, kde-format
msgid "Document Information"
msgstr "문서 정보"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: htmlpageinfo.ui:34
#, kde-format
msgctxt "@title:group Document information"
msgid "General"
msgstr "일반"

#. i18n: ectx: property (text), widget (QLabel, urlLabel)
#: htmlpageinfo.ui:60
#, kde-format
msgid "URL:"
msgstr "URL:"

#. i18n: ectx: property (text), widget (QLabel, titleLabel)
#: htmlpageinfo.ui:112
#, kde-format
msgid "Title:"
msgstr "제목:"

#. i18n: ectx: property (text), widget (QLabel, _lmLabel)
#: htmlpageinfo.ui:128
#, kde-format
msgid "Last modified:"
msgstr "마지막 수정:"

#. i18n: ectx: property (text), widget (QLabel, _eLabel)
#: htmlpageinfo.ui:144
#, kde-format
msgid "Document encoding:"
msgstr "문서 인코딩:"

#. i18n: ectx: property (text), widget (QLabel, _modeLabel)
#: htmlpageinfo.ui:167
#, kde-format
msgid "Rendering mode:"
msgstr "렌더링 모드:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: htmlpageinfo.ui:186
#, kde-format
msgid "HTTP Headers"
msgstr "HTTP 헤더"

#. i18n: ectx: property (text), widget (QTreeWidget, _headers)
#: htmlpageinfo.ui:214
#, kde-format
msgid "Property"
msgstr "속성"

#. i18n: ectx: Menu (edit)
#: khtml.rc:4 khtml_browser.rc:11
#, kde-format
msgid "&Edit"
msgstr "편집(&E)"

#. i18n: ectx: Menu (file)
#: khtml_browser.rc:4
#, kde-format
msgid "&File"
msgstr "파일(&F)"

#. i18n: ectx: Menu (view)
#: khtml_browser.rc:18
#, kde-format
msgid "&View"
msgstr "보기(&V)"

#. i18n: ectx: ToolBar (htmlToolBar)
#: khtml_browser.rc:34
#, kde-format
msgid "HTML Toolbar"
msgstr "HTML 도구 모음"

#: khtml_ext.cpp:413
#, kde-format
msgid "&Copy Text"
msgstr "텍스트 복사(&C)"

#: khtml_ext.cpp:427
#, kde-format
msgid "Open '%1'"
msgstr "'%1' 열기"

#: khtml_ext.cpp:444
#, kde-format
msgid "&Copy Email Address"
msgstr "이메일 주소 복사(&C)"

#: khtml_ext.cpp:449
#, kde-format
msgid "&Save Link As..."
msgstr "링크를 다른 이름으로 저장(&S)..."

#: khtml_ext.cpp:454
#, kde-format
msgid "&Copy Link Address"
msgstr "링크 주소 복사(&C)"

#: khtml_ext.cpp:466
#, kde-format
msgctxt "@title:menu HTML frame/iframe"
msgid "Frame"
msgstr "프레임"

#: khtml_ext.cpp:467
#, kde-format
msgid "Open in New &Window"
msgstr "새 창에서 열기(&W)"

#: khtml_ext.cpp:473
#, kde-format
msgid "Open in &This Window"
msgstr "이 창에서 열기(&T)"

#: khtml_ext.cpp:478
#, kde-format
msgid "Open in &New Tab"
msgstr "새 탭에서 열기(&N)"

#: khtml_ext.cpp:488
#, kde-format
msgid "Reload Frame"
msgstr "프레임 새로 고침"

#: khtml_ext.cpp:493 khtml_part.cpp:446
#, kde-format
msgid "Print Frame..."
msgstr "프레임 인쇄..."

#: khtml_ext.cpp:499 khtml_part.cpp:298
#, kde-format
msgid "Save &Frame As..."
msgstr "다른 이름으로 프레임 저장(&F)..."

#: khtml_ext.cpp:504 khtml_part.cpp:274
#, kde-format
msgid "View Frame Source"
msgstr "프레임 소스 보기"

#: khtml_ext.cpp:509
#, kde-format
msgid "View Frame Information"
msgstr "프레임 정보 보기"

#: khtml_ext.cpp:519
#, kde-format
msgid "Block IFrame..."
msgstr "IFrame 차단..."

#: khtml_ext.cpp:543
#, kde-format
msgid "Save Image As..."
msgstr "다른 이름으로 그림 저장..."

#: khtml_ext.cpp:548
#, kde-format
msgid "Send Image..."
msgstr "그림 보내기..."

#: khtml_ext.cpp:554
#, kde-format
msgid "Copy Image"
msgstr "그림 복사"

#: khtml_ext.cpp:562
#, kde-format
msgid "Copy Image Location"
msgstr "그림 위치 복사"

#: khtml_ext.cpp:571
#, kde-format
msgid "View Image (%1)"
msgstr "그림 보기 (%1)"

#: khtml_ext.cpp:577
#, kde-format
msgid "Block Image..."
msgstr "그림 보지 않기..."

#: khtml_ext.cpp:584
#, kde-format
msgid "Block Images From %1"
msgstr "%1의 그림 보지 않기"

#: khtml_ext.cpp:596
#, kde-format
msgid "Stop Animations"
msgstr "애니메이션 멈추기"

#: khtml_ext.cpp:637
#, kde-format
msgid "Search for '%1' with %2"
msgstr "%2에서 '%1' 찾기"

#: khtml_ext.cpp:647
#, kde-format
msgid "Search for '%1' with"
msgstr "다음으로 '%1' 찾기"

#: khtml_ext.cpp:693
#, kde-format
msgid "Save Link As"
msgstr "링크를 다른 이름으로 저장"

#: khtml_ext.cpp:712
#, kde-format
msgid "Save Image As"
msgstr "그림을 다른 이름으로 저장"

#: khtml_ext.cpp:726 khtml_ext.cpp:738
#, kde-format
msgid "Add URL to Filter"
msgstr "필터에 URL 추가"

#: khtml_ext.cpp:727 khtml_ext.cpp:739
#, kde-format
msgid "Enter the URL:"
msgstr "URL을 입력하십시오:"

#: khtml_ext.cpp:875
#, kde-format
msgid ""
"A file named \"%1\" already exists. Are you sure you want to overwrite it?"
msgstr "파일 \"%1\"이(가) 이미 존재합니다. 덮어쓰시겠습니까?"

#: khtml_ext.cpp:875
#, kde-format
msgid "Overwrite File?"
msgstr "파일을 덮어쓰시겠습니까?"

#: khtml_ext.cpp:927
#, kde-format
msgid "The Download Manager (%1) could not be found in your $PATH "
msgstr "다운로드 관리자(%1)을(를) $PATH에서 찾을 수 없습니다 "

#: khtml_ext.cpp:928
#, kde-format
msgid ""
"Try to reinstall it  \n"
"\n"
"The integration with Konqueror will be disabled."
msgstr ""
"다음을 다시 설치해 보십시오. \n"
"\n"
"컹커러 통합을 사용할 수 없습니다."

#: khtml_ext.cpp:1007
#, no-c-format, kde-format
msgid "Default Font Size (100%)"
msgstr "기본 글꼴 크기 (100%)"

#: khtml_ext.cpp:1021
#, no-c-format, kde-format
msgid "%1%"
msgstr "%1%"

#: khtml_global.cpp:203
#, kde-format
msgid "KHTML"
msgstr "KHTML"

#: khtml_global.cpp:204
#, kde-format
msgid "Embeddable HTML component"
msgstr "내장될 수 있는 HTML 요소"

#: khtml_part.cpp:267
#, kde-format
msgid "View Do&cument Source"
msgstr "문서 소스 보기(&C)"

#: khtml_part.cpp:281
#, kde-format
msgid "View Document Information"
msgstr "문서 정보 보기"

#: khtml_part.cpp:288
#, kde-format
msgid "Save &Background Image As..."
msgstr "배경 그림을 다른 이름으로 저장(&B)..."

#: khtml_part.cpp:310 khtml_part.cpp:4189
#, kde-format
msgid "SSL"
msgstr "SSL"

#: khtml_part.cpp:314
#, kde-format
msgid "Print Rendering Tree to STDOUT"
msgstr "표현 트리를 표준 출력으로 인쇄"

#: khtml_part.cpp:318
#, kde-format
msgid "Print DOM Tree to STDOUT"
msgstr "DOM 트리를 표준 출력으로 인쇄"

#: khtml_part.cpp:322
#, kde-format
msgid "Print frame tree to STDOUT"
msgstr "프레임 트리를 표준 출력으로 인쇄"

#: khtml_part.cpp:326
#, kde-format
msgid "Stop Animated Images"
msgstr "움직이는 그림 멈추기"

#: khtml_part.cpp:330
#, kde-format
msgid "Set &Encoding"
msgstr "인코딩 설정(&E)"

#: khtml_part.cpp:376
#, kde-format
msgid "Use S&tylesheet"
msgstr "스타일시트 사용(&T)"

#: khtml_part.cpp:381
#, kde-format
msgid "Enlarge Font"
msgstr "글꼴 크기 확대"

#: khtml_part.cpp:384
#, kde-format
msgid ""
"<qt>Enlarge Font<br /><br />Make the font in this window bigger. Click and "
"hold down the mouse button for a menu with all available font sizes.</qt>"
msgstr ""
"<qt>글꼴 크기 확대<br /><br />이 창 안에 있는 글꼴을 크게 만듭니다. 사용 가능"
"한 모든 글꼴 크기를 보려면 마우스 단추를 클릭한 채로 있으십시오.</qt>"

#: khtml_part.cpp:388
#, kde-format
msgid "Shrink Font"
msgstr "글꼴 크기 축소"

#: khtml_part.cpp:391
#, kde-format
msgid ""
"<qt>Shrink Font<br /><br />Make the font in this window smaller. Click and "
"hold down the mouse button for a menu with all available font sizes.</qt>"
msgstr ""
"<qt>글꼴 크기 축소<br /><br />이 창 안의 글꼴을 작게 만듭니다. 모든 사용 가능"
"한 글꼴 크기를 보려면 마우스 단추를 클릭한 채로 있으십시오.</qt>"

#: khtml_part.cpp:406
#, kde-format
msgid ""
"<qt>Find text<br /><br />Shows a dialog that allows you to find text on the "
"displayed page.</qt>"
msgstr ""
"<qt>텍스트 찾기<br /><br />표시된 페이지에서 텍스트를 찾을 수 있는 대화 상자"
"를 보여줍니다.</qt>"

#: khtml_part.cpp:410
#, kde-format
msgid ""
"<qt>Find next<br /><br />Find the next occurrence of the text that you have "
"found using the <b>Find Text</b> function.</qt>"
msgstr ""
"<qt>다음 찾기<br /><br /><b>텍스트 찾기</b> 기능을 사용하여 찾은 다음 텍스트"
"를 찾아갑니다.</qt>"

#: khtml_part.cpp:416
#, kde-format
msgid ""
"<qt>Find previous<br /><br />Find the previous occurrence of the text that "
"you have found using the <b>Find Text</b> function.</qt>"
msgstr ""
"<qt>이전 찾기<br /><br /><b>텍스트 찾기</b> 기능을 사용하여 찾은 이전 텍스트"
"를 찾아갑니다.</qt>"

#: khtml_part.cpp:421
#, kde-format
msgid "Find Text as You Type"
msgstr "입력하는 대로 텍스트 찾기"

#: khtml_part.cpp:424
#, kde-format
msgid ""
"This shortcut shows the find bar, for finding text in the displayed page. It "
"cancels the effect of \"Find Links as You Type\", which sets the \"Find "
"links only\" option."
msgstr ""
"이 키를 누르면 보고 있는 페이지의 텍스트를 찾는 데 사용하는 검색 표시줄을 표"
"시합니다. \"링크만 찿기\" 옵션을 설정하는 \"입력하는 대로 텍스트 찾기\" 옵션"
"의 효과가 취소됩니다."

#: khtml_part.cpp:428
#, kde-format
msgid "Find Links as You Type"
msgstr "입력하는 대로 링크 찾기"

#: khtml_part.cpp:434
#, kde-format
msgid ""
"This shortcut shows the find bar, and sets the option \"Find links only\"."
msgstr "검색 표시줄을 표시하며 \"링크만 찾기\" 옵션을 설정합니다."

#: khtml_part.cpp:450
#, kde-format
msgid ""
"<qt>Print Frame<br /><br />Some pages have several frames. To print only a "
"single frame, click on it and then use this function.</qt>"
msgstr ""
"<qt>프레임 인쇄<br /><br />어떤 페이지는 여러 프레임을 갖고 있습니다. 한 프레"
"임만을 인쇄하려면 프레임을 클릭한 다음 이 기능을 사용하십시오.</qt>"

#: khtml_part.cpp:465
#, kde-format
msgid "Toggle Caret Mode"
msgstr "캐럿 모드 전환"

#: khtml_part.cpp:768
#, kde-format
msgid "The fake user-agent '%1' is in use."
msgstr ""
"가상 사용자 에이전트 '%1'을(를) 사용하고 있습니다.|/|가상 사용자 에이전트 "
"'%1'$[을를 %1] 사용하고 있습니다."

#: khtml_part.cpp:1272
#, kde-format
msgid "This web page contains coding errors."
msgstr "이 웹 페이지의 소스 코드에 오류가 있습니다."

#: khtml_part.cpp:1313
#, kde-format
msgid "&Hide Errors"
msgstr "오류 숨기기(&H)"

#: khtml_part.cpp:1314
#, kde-format
msgid "&Disable Error Reporting"
msgstr "오류 알림 사용하지 않기(&D)"

#: khtml_part.cpp:1357
#, kde-format
msgid "<qt><b>Error</b>: %1: %2</qt>"
msgstr "<qt><b>오류</b>: %1: %2</qt>"

#: khtml_part.cpp:1406
#, kde-format
msgid "<qt><b>Error</b>: node %1: %2</qt>"
msgstr "<qt><b>오류</b>: 노드 %1: %2</qt>"

#: khtml_part.cpp:1528
#, kde-format
msgid "Display Images on Page"
msgstr "페이지 상의 그림 표시"

#: khtml_part.cpp:1912
#, kde-format
msgid "Error: %1 - %2"
msgstr "오류: %1: %2"

#: khtml_part.cpp:1917
#, kde-format
msgid "The requested operation could not be completed"
msgstr "요청한 작업을 완료할 수 없었습니다."

#: khtml_part.cpp:1923
#, kde-format
msgid "Technical Reason: "
msgstr "기술적 이유: "

#: khtml_part.cpp:1929
#, kde-format
msgid "Details of the Request:"
msgstr "요청의 자세한 사항:"

#: khtml_part.cpp:1931
#, kde-format
msgid "URL: %1"
msgstr "URL: %1"

#: khtml_part.cpp:1934
#, kde-format
msgid "Protocol: %1"
msgstr "프로토콜: %1"

#: khtml_part.cpp:1937
#, kde-format
msgid "Date and Time: %1"
msgstr "날짜와 시간: %1"

#: khtml_part.cpp:1939
#, kde-format
msgid "Additional Information: %1"
msgstr "추가적인 정보: %1"

#: khtml_part.cpp:1941
#, kde-format
msgid "Description:"
msgstr "설명:"

#: khtml_part.cpp:1947
#, kde-format
msgid "Possible Causes:"
msgstr "가능한 이유:"

#: khtml_part.cpp:1954
#, kde-format
msgid "Possible Solutions:"
msgstr "가능한 해결 방법:"

#: khtml_part.cpp:2402
#, kde-format
msgid "Page loaded."
msgstr "페이지를 불러왔습니다."

#: khtml_part.cpp:2404
#, kde-format
msgid "%1 Image of %2 loaded."
msgid_plural "%1 Images of %2 loaded."
msgstr[0] "그림 %2개 중 %1개 불러옴."

#: khtml_part.cpp:2582
#, kde-format
msgid "Automatic Detection"
msgstr "자동 감지"

#: khtml_part.cpp:3712 khtml_part.cpp:3776 khtml_part.cpp:3786
#, kde-format
msgid " (In new window)"
msgstr " (새 창으로)"

#: khtml_part.cpp:3740
#, kde-format
msgid "Symbolic Link"
msgstr "심볼릭 링크"

#: khtml_part.cpp:3742
#, kde-format
msgid "%1 (Link)"
msgstr "%1 (링크)"

#: khtml_part.cpp:3758
#, kde-format
msgid "%2 (%1 byte)"
msgid_plural "%2 (%1 bytes)"
msgstr[0] "%2(%1바이트)"

#: khtml_part.cpp:3761
#, kde-format
msgid "%2 (%1 K)"
msgstr "%2 (%1 K)"

#: khtml_part.cpp:3788
#, kde-format
msgid " (In other frame)"
msgstr " (다른 프레임으로)"

#: khtml_part.cpp:3794
#, kde-format
msgid "Email to: "
msgstr "받는 사람: "

#: khtml_part.cpp:3800
#, kde-format
msgid " - Subject: "
msgstr " - 제목: "

#: khtml_part.cpp:3802
#, kde-format
msgid " - CC: "
msgstr " - 참조: "

#: khtml_part.cpp:3804
#, kde-format
msgid " - BCC: "
msgstr " - 숨은 참조: "

#: khtml_part.cpp:3885 khtml_part.cpp:4107 khtml_part.cpp:4543
#: khtml_run.cpp:106
#, kde-format
msgid "Save As"
msgstr "다른 이름으로 저장"

#: khtml_part.cpp:3890
#, kde-format
msgid ""
"<qt>This untrusted page links to<br /><b>%1</b>.<br />Do you want to follow "
"the link?</qt>"
msgstr ""
"<qt>이 신뢰할 수 없는 페이지는 <br/><b>%1</b>(으)로 연결됩니다.<br />링크를 "
"따라가시겠습니까?</qt>"

#: khtml_part.cpp:3891
#, kde-format
msgid "Follow"
msgstr "따라가기"

#: khtml_part.cpp:3986
#, kde-format
msgid "Frame Information"
msgstr "프레임 정보"

#: khtml_part.cpp:3992
#, kde-format
msgid "   <a href=\"%1\">[Properties]</a>"
msgstr "   <a href=\"%1\">[속성]</a>"

#: khtml_part.cpp:4018
#, kde-format
msgctxt "HTML rendering mode (see https://en.wikipedia.org/wiki/Quirks_mode)"
msgid "Quirks"
msgstr "호환성 모드"

#: khtml_part.cpp:4021
#, kde-format
msgctxt "HTML rendering mode (see https://en.wikipedia.org/wiki/Quirks_mode)"
msgid "Almost standards"
msgstr "표준에 준하는 모드"

#: khtml_part.cpp:4025
#, kde-format
msgctxt "HTML rendering mode (see https://en.wikipedia.org/wiki/Quirks_mode)"
msgid "Strict"
msgstr "표준 모드"

#: khtml_part.cpp:4094
#, kde-format
msgid "Save Background Image As"
msgstr "배경 그림을 다른 이름으로 저장"

#: khtml_part.cpp:4187
#, kde-format
msgid "The peer SSL certificate chain appears to be corrupt."
msgstr "동료 SSL 인증서 체인이 잘못된 것 같습니다."

#: khtml_part.cpp:4208
#, kde-format
msgid "Save Frame As"
msgstr "프레임을 다른 이름으로 저장"

#: khtml_part.cpp:4252
#, kde-format
msgid "&Find in Frame..."
msgstr "프레임 안에서 찾기(&F)..."

#: khtml_part.cpp:4254
#, kde-format
msgid "&Find..."
msgstr "찾기(&F)..."

#: khtml_part.cpp:4901
#, kde-format
msgid ""
"Warning:  This is a secure form but it is attempting to send your data back "
"unencrypted.\n"
"A third party may be able to intercept and view this information.\n"
"Are you sure you wish to continue?"
msgstr ""
"경고: 이것은 보안이 유지된 폼이지만 데이터를 암호화하지 않고 전송하려고 하고 "
"있습니다.\n"
"다른 사람이 이 정보를 중간에 가로채서 볼 수 있습니다.\n"
"계속 진행하시겠습니까?"

#: khtml_part.cpp:4904 khtml_part.cpp:4914 khtml_part.cpp:4939
#, kde-format
msgid "Network Transmission"
msgstr "네트워크 전송"

#: khtml_part.cpp:4904 khtml_part.cpp:4915
#, kde-format
msgid "&Send Unencrypted"
msgstr "암호화하지 않고 전송(&S)"

#: khtml_part.cpp:4912
#, kde-format
msgid ""
"Warning: Your data is about to be transmitted across the network "
"unencrypted.\n"
"Are you sure you wish to continue?"
msgstr ""
"경고: 자료가 암호화되지 않은 채로 네트워크를 통해서 전송됩니다.\n"
"계속 진행하시겠습니까?"

#: khtml_part.cpp:4937
#, kde-format
msgid ""
"This site is attempting to submit form data via email.\n"
"Do you want to continue?"
msgstr ""
"이 사이트에서 이메일을 통해 폼 데이터를 전달하려고 하고 있습니다.\n"
"계속 진행하시겠습니까?"

#: khtml_part.cpp:4940
#, kde-format
msgid "&Send Email"
msgstr "이메일 보내기(&S)"

#: khtml_part.cpp:4961
#, kde-format
msgid ""
"<qt>The form will be submitted to <br /><b>%1</b><br />on your local "
"filesystem.<br />Do you want to submit the form?</qt>"
msgstr ""
"<qt>현재 폼은 로컬 파일 시스템의 <br /><b>%1</b><br />(으)로 전달됩니다. "
"<br />폼을 전달하시겠습니까?</qt>"

#: khtml_part.cpp:5017
#, kde-format
msgid ""
"This site attempted to attach a file from your computer in the form "
"submission. The attachment was removed for your protection."
msgstr ""
"이 사이트에서 폼 전달을 통해 컴퓨터에 있는 파일 전송을 시도했습니다. 시스템"
"을 보호할 수 있도록 첨부 파일을 삭제했습니다."

#: khtml_part.cpp:6111
#, kde-format
msgid "(%1/s)"
msgstr "(%1/초)"

#: khtml_part.cpp:7039
#, kde-format
msgid "Security Warning"
msgstr "보안 경고"

#: khtml_part.cpp:7046
#, kde-format
msgid "<qt>Access by untrusted page to<br /><b>%1</b><br /> denied.</qt>"
msgstr ""
"<qt>신뢰할 수 없는 페이지에 의한<br /><b>%1</b><br />위 대상 접근이 거부되었"
"습니다.</qt>"

#: khtml_part.cpp:7047
#, kde-format
msgid "Security Alert"
msgstr "보안 경고"

#: khtml_part.cpp:7417
#, kde-format
msgid "The wallet '%1' is open and being used for form data and passwords."
msgstr ""
"지갑 '%1'이 열려 있으며, 폼 데이터와 암호를 저장하는 데 사용하고 있습니다."

#: khtml_part.cpp:7476
#, kde-format
msgid "&Close Wallet"
msgstr "지갑 닫기(&C)"

#: khtml_part.cpp:7479
#, kde-format
msgid "&Allow storing passwords for this site"
msgstr "이 사이트의 암호 저장하기(&A)"

#: khtml_part.cpp:7484
#, kde-format
msgid "Remove password for form %1"
msgstr "양식 %1의 암호 지우기"

#: khtml_part.cpp:7599
#, kde-format
msgid "JavaScript &Debugger"
msgstr "자바스크립트 디버거(&D)"

#: khtml_part.cpp:7632
#, kde-format
msgid "This page was prevented from opening a new window via JavaScript."
msgstr "이 페이지에서 자바스크립트를 통해 새 창을 여는 것이 방지되어 있습니다."

#: khtml_part.cpp:7638
#, kde-format
msgid "Popup Window Blocked"
msgstr "팝업 창 차단됨"

#: khtml_part.cpp:7638
#, kde-format
msgid ""
"This page has attempted to open a popup window but was blocked.\n"
"You can click on this icon in the status bar to control this behavior\n"
"or to open the popup."
msgstr ""
"이 페이지에서 팝업 창을 열려고 했지만 차단되었습니다.\n"
"상태 표시줄에 있는 아이콘을 클릭해서 동작을 조정하거나\n"
"팝업 창을 열 수 있습니다."

#: khtml_part.cpp:7652
#, kde-format
msgid "&Show Blocked Popup Window"
msgid_plural "&Show %1 Blocked Popup Windows"
msgstr[0] "차단된 팝업 창 %1개 보기(&S)"

#: khtml_part.cpp:7654
#, kde-format
msgid "Show Blocked Window Passive Popup &Notification"
msgstr "차단된 수동 팝업 알림 보기(&N)"

#: khtml_part.cpp:7656
#, kde-format
msgid "&Configure JavaScript New Window Policies..."
msgstr "자바스크립트 새 창 정책 설정(&C)..."

#: khtml_printsettings.cpp:30
#, kde-format
msgid ""
"<qt><p><strong>'Print images'</strong></p><p>If this checkbox is enabled, "
"images contained in the HTML page will be printed. Printing may take longer "
"and use more ink or toner.</p><p>If this checkbox is disabled, only the text "
"of the HTML page will be printed, without the included images. Printing will "
"be faster and use less ink or toner.</p> </qt>"
msgstr ""
"<qt><p><strong>'그림 인쇄'</strong></p><p>만약 이 설정을 사용하면 HTML 페이지"
"에 들어 있는 그림을 인쇄합니다. 인쇄 시간이 더 오래 걸릴 것이며 더 많은 잉크"
"나 토너를 사용합니다. </p><p>이 설정을 사용하지 않으면 HTML 문서의 텍스트만 "
"인쇄되며, 그림 등은 제외합니다. 인쇄 속도는 빨라지고 잉크나 토너를 적게 사용"
"합니다.</p></qt>"

#: khtml_printsettings.cpp:42
#, kde-format
msgid ""
"<qt><p><strong>'Print header'</strong></p><p>If this checkbox is enabled, "
"the printout of the HTML document will contain a header line at the top of "
"each page. This header contains the current date, the location URL of the "
"printed page and the page number.</p><p>If this checkbox is disabled, the "
"printout of the HTML document will not contain such a header line.</p> </qt>"
msgstr ""
"<qt><p><strong>'인쇄 머리말'</strong></p><p>만약 이 설정을 사용하면, HTML 문"
"서의 각 쪽 위에 머리말 줄을 인쇄합니다. 머리말에는 오늘 날짜, 인쇄된 문서의 "
"URL과 쪽 번호가 들어 있습니다.</p><p>이 설정을 사용하지 않으면 HTML 문서에 머"
"리말을 출력하지 않습니다.</p> </qt>"

#: khtml_printsettings.cpp:55
#, kde-format
msgid ""
"<qt><p><strong>'Printerfriendly mode'</strong></p><p>If this checkbox is "
"enabled, the printout of the HTML document will be black and white only, and "
"all colored background will be converted into white. Printout will be faster "
"and use less ink or toner.</p><p>If this checkbox is disabled, the printout "
"of the HTML document will happen in the original color settings as you see "
"in your application. This may result in areas of full-page color (or "
"grayscale, if you use a black+white printer). Printout will possibly happen "
"more slowly and will probably use more toner or ink.</p> </qt>"
msgstr ""
"<qt><p><strong>'프린터 친화 모드'</strong></p><p>만약 이 설정을 사용하면 "
"HTML 문서는 흑백으로 출력되며, 모든 색이 있는 배경은 흰색으로 변환됩니다. 출"
"력 과정은 더 빨라지며, 잉크나 토너를 절약할 수 있습니다.</p><p>이 설정을 사용"
"하지 않으면, HTML 문서는 프로그램에서 보았던 원본 색상 설정 그대로 출력됩니"
"다. 이것은 모든 쪽을 컬러로 (흑백 프린터를 사용한다면 흑백으로) 출력합니다. "
"출력 속도가 느려질 수 있으며, 더 많은 잉크나 토너를 사용합니다.</p> </qt>"

#: khtml_printsettings.cpp:70
#, kde-format
msgid "HTML Settings"
msgstr "HTML 설정"

#: khtml_printsettings.cpp:72
#, kde-format
msgid "Printer friendly mode (black text, no background)"
msgstr "프린터 친화 모드 (흑색 본문, 배경 없음)"

#: khtml_printsettings.cpp:75
#, kde-format
msgid "Print images"
msgstr "그림 인쇄"

#: khtml_printsettings.cpp:78
#, kde-format
msgid "Print header"
msgstr "인쇄 머리말"

#: khtml_settings.cpp:906
#, kde-format
msgid "Filter error"
msgstr "필터 오류"

#: khtmladaptorpart.cpp:29
#, kde-format
msgid "Inactive"
msgstr "비활성"

#: khtmlimage.cpp:49
#, kde-format
msgid "KHTML Image"
msgstr "KHTML 그림"

#: khtmlimage.cpp:195
#, kde-format
msgid "%1 (%2 - %3x%4 Pixels)"
msgstr "%1 - (%2 - %3x%4 픽셀)"

#: khtmlimage.cpp:197
#, kde-format
msgid "%1 - %2x%3 Pixels"
msgstr "%1 - %2x%3 픽셀"

#: khtmlimage.cpp:202
#, kde-format
msgid "%1 (%2x%3 Pixels)"
msgstr "%1 (%2x%3 픽셀)"

#: khtmlimage.cpp:204
#, kde-format
msgid "Image - %1x%2 Pixels"
msgstr "이미지 - %1x%2 픽셀"

#: khtmlimage.cpp:210
#, kde-format
msgid "Done."
msgstr "마침."

#: khtmlview.cpp:1903
#, kde-format
msgid "Access Keys activated"
msgstr "접근 키가 활성화되었습니다"

#: kjserrordlg.cpp:9
#, kde-format
msgid "C&lear"
msgstr "지우기(&L)"

#. i18n: ectx: property (windowTitle), widget (QDialog, KJSErrorDlgBase)
#: kjserrordlgbase.ui:15
#, kde-format
msgid "JavaScript Errors"
msgstr "자바스크립트 오류"

#. i18n: ectx: property (whatsThis), widget (QDialog, KJSErrorDlgBase)
#: kjserrordlgbase.ui:18
#, kde-format
msgid ""
"This dialog provides you with notification and details of scripting errors "
"that occur on web pages.  In many cases it is due to an error in the web "
"site as designed by its author.  In other cases it is the result of a "
"programming error in Konqueror.  If you suspect the former, please contact "
"the webmaster of the site in question.  Conversely if you suspect an error "
"in Konqueror, please file a bug report at https://bugs.kde.org/.  A test "
"case which illustrates the problem will be appreciated."
msgstr ""
"이 대화 상자는 웹 페이지에 있는 스크립트 오류에 대한 자세한 정보와 경고를 표"
"시합니다. 대부분의 경우 웹사이트 제작자의 설계 오류 때문에 발생하는 것입니"
"다. 그렇지 않다면 Konqueror의 프로그램 오류일 수 있습니다. 전자라고 생각하면 "
"사이트 관리자에게 질문을 보내십시오. 반대로 Konqueror의 내부 오류라고 생각하"
"면 https://bugs.kde.org/ 사이트에 버그를 알려 주십시오. 문제를 다시 재현할 "
"수 있는 예제도 같이 설명해서 보내 주십시오."

#: kmultipart/httpfiltergzip.cpp:89
#, kde-format
msgid "Receiving corrupt data."
msgstr "잘못된 데이터를 받았습니다."

#: kmultipart/kmultipart.cpp:39
#, kde-format
msgid "KMultiPart"
msgstr "KMultiPart"

#: kmultipart/kmultipart.cpp:41
#, kde-format
msgid "Embeddable component for multipart/mixed"
msgstr "multipart/mixed용 임베드 가능 구성 요소"

#: kmultipart/kmultipart.cpp:43
#, kde-format
msgid "Copyright 2001-2011, David Faure <faure@kde.org>"
msgstr "Copyright 2001-2011, David Faure <faure@kde.org>"

#: kmultipart/kmultipart.cpp:335
#, kde-format
msgid "No handler found for %1."
msgstr "%1 핸들러를 찾을 수 없습니다."

#: misc/kencodingdetector.cpp:1123 misc/kencodingdetector.cpp:1224
#, kde-format
msgctxt "@item Text character set"
msgid "Unicode"
msgstr "유니코드"

#: misc/kencodingdetector.cpp:1125 misc/kencodingdetector.cpp:1194
#, kde-format
msgctxt "@item Text character set"
msgid "Cyrillic"
msgstr "키릴 문자"

#: misc/kencodingdetector.cpp:1127 misc/kencodingdetector.cpp:1209
#, kde-format
msgctxt "@item Text character set"
msgid "Western European"
msgstr "서부 유럽 문자"

#: misc/kencodingdetector.cpp:1129 misc/kencodingdetector.cpp:1191
#, kde-format
msgctxt "@item Text character set"
msgid "Central European"
msgstr "중부 유럽 문자"

#: misc/kencodingdetector.cpp:1131 misc/kencodingdetector.cpp:1197
#, kde-format
msgctxt "@item Text character set"
msgid "Greek"
msgstr "그리스 문자"

#: misc/kencodingdetector.cpp:1133 misc/kencodingdetector.cpp:1200
#, kde-format
msgctxt "@item Text character set"
msgid "Hebrew"
msgstr "히브리 문자"

#: misc/kencodingdetector.cpp:1135 misc/kencodingdetector.cpp:1206
#, kde-format
msgctxt "@item Text character set"
msgid "Turkish"
msgstr "튀르키예 문자"

#: misc/kencodingdetector.cpp:1137 misc/kencodingdetector.cpp:1203
#, kde-format
msgctxt "@item Text character set"
msgid "Japanese"
msgstr "가나"

#: misc/kencodingdetector.cpp:1139 misc/kencodingdetector.cpp:1188
#, kde-format
msgctxt "@item Text character set"
msgid "Baltic"
msgstr "발트 문자"

#: misc/kencodingdetector.cpp:1141 misc/kencodingdetector.cpp:1185
#, kde-format
msgctxt "@item Text character set"
msgid "Arabic"
msgstr "아라비아 문자"

#: misc/kencodingdetector.cpp:1212
#, kde-format
msgctxt "@item Text character set"
msgid "Chinese Traditional"
msgstr "중국 번자체"

#: misc/kencodingdetector.cpp:1215
#, kde-format
msgctxt "@item Text character set"
msgid "Chinese Simplified"
msgstr "중국 간자체"

#: misc/kencodingdetector.cpp:1218
#, kde-format
msgctxt "@item Text character set"
msgid "Korean"
msgstr "한글"

#: misc/kencodingdetector.cpp:1221
#, kde-format
msgctxt "@item Text character set"
msgid "Thai"
msgstr "태국 문자"

#: rendering/media_controls.cpp:50
#, kde-format
msgid "Play"
msgstr "재생"

#: rendering/media_controls.cpp:53
#, kde-format
msgid "Pause"
msgstr "일시 정지"

#: rendering/render_form.cpp:901
#, kde-format
msgid "New Web Shortcut"
msgstr "새 웹 바로 가기"

#: rendering/render_form.cpp:922
#, kde-format
msgid "%1 is already assigned to %2"
msgstr "%1이(가) 이미 %2에 할당되어 있습니다"

#: rendering/render_form.cpp:968
#, kde-format
msgid "Search &provider name:"
msgstr "공급자 이름 입력(&P):"

#: rendering/render_form.cpp:970
#, kde-format
msgid "New search provider"
msgstr "새 검색 공급자"

#: rendering/render_form.cpp:975
#, kde-format
msgid "UR&I shortcuts:"
msgstr "URI 바로 가기(&I):"

#: rendering/render_form.cpp:1054
#, kde-format
msgid "Clear &History"
msgstr "과거 기록 삭제(&H)"

#: rendering/render_form.cpp:1069
#, kde-format
msgid "Create Web Shortcut"
msgstr "웹 바로 가기 만들기"

#: ui/findbar/khtmlfindbar.cpp:49
#, kde-format
msgid "C&ase sensitive"
msgstr "대소문자 구분(&A)"

#: ui/findbar/khtmlfindbar.cpp:51
#, kde-format
msgid "&Whole words only"
msgstr "단어 단위로(&W)"

#: ui/findbar/khtmlfindbar.cpp:53
#, kde-format
msgid "From c&ursor"
msgstr "커서에서부터(&U)"

#: ui/findbar/khtmlfindbar.cpp:55
#, kde-format
msgid "&Selected text"
msgstr "선택한 문자열(&S)"

#: ui/findbar/khtmlfindbar.cpp:57
#, kde-format
msgid "Regular e&xpression"
msgstr "정규 표현식(&X)"

#: ui/findbar/khtmlfindbar.cpp:59
#, kde-format
msgid "Find &links only"
msgstr "링크만 찾기(&L)"

#: ui/findbar/khtmlfindbar.cpp:224
#, kde-format
msgid "Not found"
msgstr "찾지 못했습니다"

#: ui/findbar/khtmlfindbar.cpp:239
#, kde-format
msgid "No more matches for this search direction."
msgstr "이 방향으로 더 이상 일치하는 것이 없습니다."

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/findbar/khtmlfindbar_base.ui:43
#, kde-format
msgid "F&ind:"
msgstr "찾기(&I):"

#. i18n: ectx: property (text), widget (QToolButton, m_next)
#: ui/findbar/khtmlfindbar_base.ui:75
#, kde-format
msgid "&Next"
msgstr "다음(&N)"

#. i18n: ectx: property (text), widget (QToolButton, m_previous)
#: ui/findbar/khtmlfindbar_base.ui:85
#, kde-format
msgid "&Previous"
msgstr "이전(&P)"

#. i18n: ectx: property (text), widget (QToolButton, m_options)
#: ui/findbar/khtmlfindbar_base.ui:111
#, kde-format
msgid "Opt&ions"
msgstr "옵션(&I)"

#. i18n: ectx: property (text), widget (QLabel, m_label)
#: ui/passwordbar/storepassbar.cpp:59 ui/passwordbar/storepassbar_base.ui:39
#, kde-format
msgid "Do you want to store this password?"
msgstr "이 암호를 저장하시겠습니까?"

#: ui/passwordbar/storepassbar.cpp:61
#, kde-format
msgid "Do you want to store this password for %1?"
msgstr "이 %1의 암호를 저장하시겠습니까?"

#. i18n: ectx: property (text), widget (QToolButton, m_store)
#: ui/passwordbar/storepassbar_base.ui:59
#, kde-format
msgid "&Store"
msgstr "저장(&S)"

#. i18n: ectx: property (text), widget (QToolButton, m_neverForThisSite)
#: ui/passwordbar/storepassbar_base.ui:69
#, kde-format
msgid "Ne&ver store for this site"
msgstr "이 사이트에서는 안 함(&V)"

#. i18n: ectx: property (text), widget (QToolButton, m_doNotStore)
#: ui/passwordbar/storepassbar_base.ui:79
#, kde-format
msgid "Do &not store this time"
msgstr "지금 저장 안 함(&N)"

#: xml/dom_docimpl.cpp:2376
#, kde-format
msgid "Basic Page Style"
msgstr "기본 페이지 스타일"

#: xml/xml_tokenizer.cpp:347
#, kde-format
msgid "the document is not in the correct file format"
msgstr "문서가 잘못된 파일 형식으로 되어 있습니다."

#: xml/xml_tokenizer.cpp:352
#, kde-format
msgid "fatal parsing error: %1 in line %2, column %3"
msgstr "치명적 해석 오류: 행 %2, 열 %3 안의 %1"

#: xml/xml_tokenizer.cpp:567
#, kde-format
msgid "XML parsing error"
msgstr "XML 해석 오류"

#~ msgid "Error"
#~ msgstr "오류"

#~ msgid "Initializing Applet \"%1\"..."
#~ msgstr "애플릿 \"%1\" 초기화하는 중..."

#~ msgid "Starting Applet \"%1\"..."
#~ msgstr "애플릿 \"%1\" 시작하는 중..."

#~ msgid "Applet \"%1\" started"
#~ msgstr "애플릿 \"%1\" 시작됨"

#~ msgid "Applet \"%1\" stopped"
#~ msgstr "애플릿 \"%1\" 중지됨"

#~ msgid "Loading Applet"
#~ msgstr "애플릿 불러오는 중"

#~ msgid "Error: java executable not found"
#~ msgstr "오류: java 실행 파일을 찾을 수 없습니다"

#~ msgid "Signed by (validation: %1)"
#~ msgstr "서명한 사람 (상태: %1)"

#~ msgid "Certificate (validation: %1)"
#~ msgstr "인증서 (상태: %1)"

#~ msgid "Do you grant Java applet with certificate(s):"
#~ msgstr "다음 인증서가 있는 자바 애플릿을 허가하시겠습니까?:"

#~ msgid "the following permission"
#~ msgstr "다음의 권한"

#~ msgid "&No"
#~ msgstr "아니오(&N)"

#~ msgid "&Reject All"
#~ msgstr "모두 거부(&R)"

#~ msgid "&Yes"
#~ msgstr "예(&Y)"

#~ msgid "&Grant All"
#~ msgstr "모두 허가(&G)"

#~ msgid "KDE Java Applet Plugin"
#~ msgstr "KDE 자바 애플릿 플러그인"

#~ msgid "Applet Parameters"
#~ msgstr "애플릿 인자"

#~ msgid "Parameter"
#~ msgstr "인자"

#~ msgid "Class"
#~ msgstr "클래스"

#~ msgid "Base URL"
#~ msgstr "기본 URL"

#~ msgid "Archives"
#~ msgstr "압축 파일"

#~ msgid "Overwrite"
#~ msgstr "덮어쓰기"

#~ msgid "&Close"
#~ msgstr "닫기(&C)"
